import 'package:dart_application_1/dart_application_1.dart';
import 'package:test/test.dart';
import '../bin/dart_application_1.dart';

void main() {
  test('calculate', () {
    expect(calculate(), 42);
  });

  test('juan se espera que coincida "j,u"', "j,u"){
    expect(calculate(), 42);
  }
  
}
